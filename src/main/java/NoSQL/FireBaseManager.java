package NoSQL;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FireBaseManager {
	public FireBaseManager() {
		FileInputStream serviceAccount = null;
		try {
			serviceAccount = new FileInputStream("messenger-a0c8a-firebase-adminsdk-id3pg-e7c73f212c.json");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FirebaseOptions options = null;
		try {
			options = new FirebaseOptions.Builder()
				  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
				  .setDatabaseUrl("https://messenger-a0c8a.firebaseio.com")
				  .build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		FirebaseApp.initializeApp(options);
	}
	
	public DatabaseReference getDbReference() {
		final FirebaseDatabase database = FirebaseDatabase.getInstance();
		return database.getReference("");
	}
	
	public String addUser(User user) {
		DatabaseReference usersRef = this.getDbReference().child("currentUsers");
		DatabaseReference pushedUserRef = usersRef.push();
		pushedUserRef.setValueAsync(user);
		return pushedUserRef.getKey();
	}
}
